# Chang et al 2015 reanalyses

This repository includes reanalyses of the data presented in:

Chang ES, Neuhof M, Rubinstein ND, Diamant A, Philippe H, Huchon D, Cartwright P: Genomic insights into the evolutionary origin of Myxozoa within Cnidaria. Proc. Natl. Acad. Sci. 2015, [doi:10.1073/pnas.1511468112](http://dx.doi.org/10.1073/pnas.1511468112).

This manuscript addressed the phylogenetic placement of Myxozoa. In addition to bringing new data and analyses to bear on this question, it also placed ctenophores, not sponges, as the sister group to remaining animals with maximal support (100% posterior probability):

![Chang et al. 2015 Figure 2](https://bitbucket.org/caseywdunn/chang_2015_reanalyses/raw/master/ChangFig2.png)


A couple weeks after Chang et al. was published, [Pisani et al. 2015](http://dx.doi.org/10.1073/pnas.1518127112) (which shares a co-author with Chang et al. 2015) instead claimed to find strong support for sponges as the sister group to other animals. Pisani et al. rejected the Chang et al. results as follows:

> Thus, while strong support for Ctenophora-sister may be obtained from phylogenomic datasets (2–6, 46, 55), our analysis suggests these results are caused by undetected systematic bias. 

Here I reanalyze the Chang et al. data to see if the potential biases identified by Pisani et al. can be detected in the Chang et al. data. I performed four analyses to address the following potential concerns:

- Removed Myxozoa, which had a highly accelarated rate of molecular evolution and could impact animal rooting
- Sensitivity to outgroup selection
- Sensitivity to model selection

The four analyses did not converge, but this was due to differences between runs for relationships within Cnidaria or Bilateria. All runs of all analyses recovered Ctenophores as the sister group to other animals with strong support.

These reanalyses did not find evidence of "undetected systematic bias" impacting the placement of ctenophores in Chang et al., even when accounting for the specific factors that Pisani et al. claim are responsible for artefacts in placing the root of the animal tree. This further emphasizes that the placement of ctenophores as the sister group to other animals should not be taken alone as evidence that an analysis is biased, especially when the question at hand is whether ctenophores are the sister group to other animals.

## Results

All new analyses differ from Chang et al. in that Myxozoa is excluded. 


![no-myxo](https://bitbucket.org/caseywdunn/chang_2015_reanalyses/raw/master/runs/no-myxo/bpcomp.con.svg)
Analysis no-myxo. Same outgroup sampling and model (CAT) as Chang et al.

![no-myxo-gtr](https://bitbucket.org/caseywdunn/chang_2015_reanalyses/raw/master/runs/no-myxo-gtr/bpcomp.con.svg)
Analysis no-gtr. Same outgroup sampling as Chang et al., but run under the CATGTR model.

![no-myxo_choanimalia](https://bitbucket.org/caseywdunn/chang_2015_reanalyses/raw/master/runs/no-myxo_choanimalia/bpcomp.con.svg)
Analysis no-myxo_choanimalia. Same model (CAT) as Chang et al., but outgroup sampling reduced to just choanoflagellates.

![no-myxo_choanimalia_gtr](https://bitbucket.org/caseywdunn/chang_2015_reanalyses/raw/master/runs/no-myxo_choanimalia_gtr/bpcomp.con.svg)
Analysis no-myxo_choanimalia_gtr. Outgroup sampling reduced to just choanoflagellates and run under the CATGTR model.


## Analysis notebook

The original nexus file was downloaded from [treebase](http://treebase.org/treebase-web/search/study/matrices.html?id=17743). It was then converted to phylip format with mesquite, and saved as `chang_2015.phy`. Moved anaimal outgroups to top of matrix, deleted the tree from the file. Created `chang_2015_no-myxo.phy` by deleting Myxozoa and Polypodium. These taxa have very long branches. Removing them will test if they have impact on animal rooting and may help speed convergence.

I analyzed each of these two matrices under two models (CAT and CATGTR) for a total of four analyses. Two runs were performed for each analysis.

Phylobayes analyses are in the `runs` folder.

Burnins, as determined by inspecting chain*.trace files:

	no-myxo	1500
	no-myxo-gtr	500
	no-myxo_choanimalia	1500
	no-myxo_choanimalia_gtr 500


Total number of posterior samples:

    wc -l runs/*/*trace
     2651 runs/no-myxo-gtr/chain1.trace
     2670 runs/no-myxo-gtr/chain2.trace
    11610 runs/no-myxo/chain1.trace
    11210 runs/no-myxo/chain2.trace
    12928 runs/no-myxo_choanimalia/chain1.trace
    12108 runs/no-myxo_choanimalia/chain2.trace
     2882 runs/no-myxo_choanimalia_gtr/chain1.trace
     2747 runs/no-myxo_choanimalia_gtr/chain2.trace


Commands used to assess convergence:

	module load phylobayes/1.6j-mpi
	cd runs/no-myxo
	bpcomp -x 1500 10 chain1 chain2
	cd ../no-myxo-gtr/
	bpcomp -x 500 10 chain1 chain2
	cd ../no-myxo_choanimalia
	bpcomp -x 1500 10 chain1 chain2
	cd ../no-myxo_choanimalia_gtr/
	bpcomp -x 500 10 chain1 chain2

None of the runs converged according to maxdiff (which was 1). In no case, though, was lack of convergence due to differences between runs in the root of the animal tree. It was instead due to differences in relationships within Cnidaria or Bilateria.
