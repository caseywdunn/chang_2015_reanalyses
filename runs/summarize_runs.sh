
# Burnins, as determined by inspecting chain*.trace files
# no-myxo	1500
# no-myxo-gtr	500
# no-myxo_choanimalia	1500
# no-myxo_choanimalia_gtr 500

module load phylobayes/1.6j-mpi

bpcomp -x 1500 10 <chain1> <chain2>
