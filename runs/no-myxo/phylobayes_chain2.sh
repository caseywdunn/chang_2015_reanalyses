#!/bin/bash
#SBATCH -J ChainTwo_cat
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=16
#SBATCH -t 5-00:00:00
#SBATCH -n 64
#SBATCH --mem=60G
#SBATCH --qos=epscor-condo
#SBATCH -e ChainTwo_cat-%j.out
#SBATCH -o ChainTwo_cat-%j.out

# module load phylobayes/4.1b
module load phylobayes/1.6j-mpi

# pb -cat -poisson -d ../../chang_2015_no-myxo.phy no-myxo
srun pb_mpi -dc -d ../../chang_2015_no-myxo.phy -cat -poisson chain2
