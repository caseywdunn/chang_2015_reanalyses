#!/bin/bash
#SBATCH -J ChainOne_cat
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=16
#SBATCH -t 5-00:00:00
#SBATCH -n 64
#SBATCH --mem=60G
#SBATCH --qos=epscor-condo
#SBATCH -e ChainOne_cat-%j.out
#SBATCH -o ChainOne_cat-%j.out

module load phylobayes/1.6j-mpi

srun pb_mpi -dc -d ../../chang_2015_no-myxo_choanimalia.phy -cat -poisson chain1
